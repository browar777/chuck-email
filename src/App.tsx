import React, { FunctionComponent, useState } from "react";
import { ConnectedRouter } from "connected-react-router";
import { createBrowserHistory, History } from "history";
import { Provider } from "react-redux";

import Store from "./app/store";
import Router from "./app/Router";

const App: FunctionComponent = () => {
  const history: History = createBrowserHistory();
  Store.init(history);

  return (
    <Provider store={Store.getStore()}>
      <ConnectedRouter history={history}>
        <Router />
      </ConnectedRouter>
    </Provider>
  );
};

export default App;
