import { IRequestRespon, ApiService } from "./ApiService";

interface IJoke {
  type: string;
  value: {
    id: number;
    joke: string;
    categories: any[];
  };
}

class JokeService extends ApiService {
  private baseUrl: string = `${process.env.REACT_APP_API}/jokes`;

  public getRandom(): Promise<IRequestRespon> {
    return this.get(`${this.baseUrl}/random`);
  }
}

export { JokeService };
export type { IJoke };
