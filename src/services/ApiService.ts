type requestType = "GET" | "PUT" | "POST" | "DELETE";

export interface IRequestRespon<T = any> {
  headers: any;
  status: number;
  data: T;
  success: boolean;
}

export class ApiService {
  private readonly abortController: AbortController;
  private readonly signal: AbortSignal;

  constructor() {
    this.abortController = new AbortController();
    this.signal = this.abortController.signal;
  }

  readonly headersConfig = {
    // "Content-Type": "application/json",
  };

  public abortFetch() {
    this.abortController.abort();
  }

  protected async get(url: string): Promise<IRequestRespon> {
    return this.prepareRequest("GET", url);
  }

  protected async post(url: string, body = {}): Promise<IRequestRespon> {
    return this.prepareRequest("POST", url, body);
  }

  protected async put(url: string, body = {}): Promise<IRequestRespon> {
    return this.prepareRequest("PUT", url, body);
  }

  protected async delete(url: string, body = {}): Promise<IRequestRespon> {
    return this.prepareRequest("DELETE", url);
  }

  protected async prepareRequest(
    method: requestType,
    url: string,
    body: any = null
  ): Promise<IRequestRespon> {
    const headersConfig = {
      ...this.headersConfig,
    };

    if (body != null) {
      body = JSON.stringify(body);
    }

    return this.sendRequest(method, url, body, headersConfig);
  }

  public async sendRequest(
    method: requestType,
    url: string,
    body: any = null,
    headersConfig = this.headersConfig
  ): Promise<IRequestRespon> {
    const config: RequestInit = {
      method,
      headers: new Headers(headersConfig),
      signal: this.signal,
    };

    try {
      if (body != null) {
        config.body = body;
      }

      const fetchResponse: Response = await fetch(url, config);
      const responseText: string = await fetchResponse.text();
      const responHeader: any = {};
      let data: any;

      fetchResponse.headers.forEach((element: any, index: string) => {
        responHeader[index] = element;
      });

      if (responseText) {
        data = JSON.parse(responseText);
      }

      return {
        headers: responHeader,
        status: fetchResponse.status,
        data,
        success: fetchResponse.status >= 200 && fetchResponse.status <= 204,
      };
    } catch (e) {
      console.error("[ApiService - sendRequest] fetchResponse failed", e);
      return { headers: {}, status: e?.status, data: [], success: false };
    }
  }
}
