import styled, { css } from "styled-components";
export * from "./fonts";
export { default as Colors } from "./colors";

export const DefaultBoxShadow = css`
  box-shadow: 0px 2px 6px #0000001a;
`;
