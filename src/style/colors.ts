enum Colors {
  jokeBackground = "#fff",
  mainHeaderBackground = "#464646",
  mainPageContainer = "#303030",
  mainFont = "#bbbbbb",
}

export default Colors;
