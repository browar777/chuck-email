import { createGlobalStyle } from "styled-components";

import SourceSansProRegular from "../static/fonts/SourceSansPro-Regular.ttf";
import SourceSansProLight from "../static/fonts/SourceSansPro-Light.ttf";
import SourceSansProBold from "../static/fonts/SourceSansPro-Bold.ttf";

enum Fonts {
  MainFont = "MainFont",
}

const FontGlobalStyle = createGlobalStyle`
  @font-face {
    font-family: '${Fonts.MainFont}';
    src: url(${SourceSansProLight});
    font-weight: 300;
    font-style: normal;
  }

  @font-face {
    font-family: '${Fonts.MainFont}';
    src: url(${SourceSansProRegular});
    font-weight: 500;
    font-style: normal;
  }

  @font-face {
    font-family: '${Fonts.MainFont}';
    src: url(${SourceSansProBold});
    font-weight: bold;
    font-style: normal;
  }
`;

export { Fonts, FontGlobalStyle };
