import {
  MutableRefObject,
  RefObject,
  useEffect,
  useRef,
  useState,
} from "react";
import { IRequestRespon } from "../../services/ApiService";

interface IMessages {
  success?: string;
  fail?: string;
  empty?: string;
}

const useFetch = <Service, DataType>(serviceClass: new () => Service) => {
  const serviceInstanceRef: RefObject<Service> = useRef<Service>(
    new serviceClass()
  );
  const disableToastsRef: MutableRefObject<boolean> = useRef<boolean>(false);

  const [loadingState, setLoadingState] = useState<boolean>(false);
  const [dataState, setDataState] = useState<DataType>([] as any);

  useEffect(() => {
    return () => {
      disableToastsRef.current = true;
      // serviceInstanceRef.current!["abortFetch"]();
    };
  }, []);

  const fetch = async (
    serviceCallback: () => any
  ): Promise<IRequestRespon<DataType>> => {
    if (loadingState) {
      //  serviceInstanceRef.current!["abortFetch"]();
    }
    setLoadingState(true);

    const res: IRequestRespon<DataType> = await serviceCallback();

    setDataState(res?.data);
    setLoadingState(false);

    return res;
  };

  const resetState = () => {
    setDataState([] as any);
  };

  return {
    fetch,
    loading: loadingState,
    state: dataState,
    reset: resetState,
    setState: setDataState,
    service: serviceInstanceRef.current!,
  };
};

export default useFetch;
