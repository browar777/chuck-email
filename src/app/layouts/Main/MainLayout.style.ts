import styled from "styled-components";
import { Colors, DefaultBoxShadow, Fonts } from "../../../style";

const layoutHeaderHeight: number = 70;

export const MainLayout = styled.div`
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  flex-direction: column;
  position: relative;

  .layout-header {
    ${DefaultBoxShadow};
    height: ${layoutHeaderHeight}px;
    width: 100%;
    background-color: ${Colors.mainHeaderBackground};
    position: absolute;
    top: 0;
    left: 0;
    font-family: ${Fonts.MainFont};
    padding: 0 20px;
    display: flex;
    align-items: center;
    box-sizing: border-box;
    color: ${Colors.mainFont};
  }

  .page-container {
    background-color: ${Colors.mainPageContainer};
    padding-top: ${layoutHeaderHeight}px;
    flex: 1;
    display: flex;
  }
`;
