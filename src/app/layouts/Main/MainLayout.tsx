import React, { FunctionComponent } from "react";

import * as Styled from "./MainLayout.style";
import { push } from "connected-react-router";
import { useDispatch } from "react-redux";
import { FontGlobalStyle } from "../../../style";

interface IMainLayout {
  children: JSX.Element;
}
const MainLayout: FunctionComponent<IMainLayout> = (props) => {
  const dispatch = useDispatch();
  return (
    <Styled.MainLayout>
      <FontGlobalStyle />
      {/*<ul>*/}
      {/*  <li>*/}
      {/*    <button onClick={() => dispatch(push("/main"))}>Main</button>*/}
      {/*  </li>*/}
      {/*  <li>*/}
      {/*    <button onClick={() => dispatch(push("/test"))}>Test</button>*/}
      {/*  </li>*/}
      {/*</ul>*/}

      <div className={"layout-header"}>
        <h3>Chuck E-Mail</h3>
      </div>
      <div className={"page-container"}>{props.children}</div>
    </Styled.MainLayout>
  );
};

export default MainLayout;
