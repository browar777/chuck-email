import { FunctionComponent, RefObject, useRef } from "react";
import * as Styled from "./MainPage.style";
import { DefaultButton, EmailTable, Joke } from "../../components";
import { ICollectEmails } from "../../components/EmailTable/EmailTable";

const MainPage: FunctionComponent = () => {
  const emailTabRef: RefObject<ICollectEmails[]> = useRef<ICollectEmails[]>([]);
  const jokeRef: RefObject<string> = useRef<string>("");

  const postEmails = () => {
    if (emailTabRef.current && jokeRef.current) {
      const postData = {
        emails: emailTabRef.current.map((collectEmail) => collectEmail.email),
        joke: jokeRef.current,
      };

      alert(JSON.stringify(postData));
    }
  };

  return (
    <Styled.MainPage>
      <div className={"table-container"}>
        <EmailTable ref={emailTabRef} />
        <br />
        <DefaultButton callback={postEmails}>Post</DefaultButton>
      </div>
      <div className={"joke-container"}>
        <Joke ref={jokeRef} />
      </div>
    </Styled.MainPage>
  );
};

export default MainPage;
