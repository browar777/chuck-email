import styled from "styled-components";

export const MainPage = styled.div`
  display: flex;
  flex: 1;

  & > div {
    padding: 30px;
    justify-content: center;
    align-items: center;
    display: flex;
  }

  .table-container {
    flex: 3;
    flex-direction: column;
  }

  .joke-container {
    flex: 2;
  }
`;
