import {
  Store as ReduxStore,
  createStore,
  applyMiddleware,
  combineReducers,
  compose,
} from "redux";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { History } from "history";

class Store {
  private static customStore: ReduxStore;
  public static init(history: History) {
    Store.customStore = createStore(
      combineReducers({
        router: connectRouter(history),
      }),
      {},
      compose(
        applyMiddleware(
          routerMiddleware(history) // for dispatching history actions
        )
      )
    );
  }

  public static getStore(): ReduxStore {
    return Store.customStore;
  }
}

export default Store;
