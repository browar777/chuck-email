import React, { FunctionComponent, useEffect, useState } from "react";
import { Route, Router as ReactRouter, Switch } from "react-router";

import { MainLayout } from "./layouts";
import { MainPage, TestPage } from "./pages";

interface INavigation {
  path: string[];
  Layout: FunctionComponent<any>;
  Page: FunctionComponent<any>;
}

const Router: FunctionComponent = () => {
  const [layoutsRoutesState, setLayoutsRoutesState] = useState<INavigation[]>(
    []
  );

  useEffect(() => {
    setLayoutsRoutesState([
      {
        path: ["/", "/main"],
        Layout: MainLayout,
        Page: MainPage,
      },
      {
        path: ["/test"],
        Layout: MainLayout,
        Page: TestPage,
      },
    ]);
  }, []);

  const renderRouts = (): JSX.Element[] => {
    return layoutsRoutesState.map(({ path, Page, Layout }) => (
      <Route
        path={[...path]}
        exact={true}
        key={`layout-${path[0]}-routing-key`}
        render={() => (
          <Layout>
            <Page />
          </Layout>
        )}
      />
    ));
  };

  return (
    <Switch>
      {renderRouts()}
      <Route
        key={`404-last-routing-element`}
        render={() => <h1>404-last-routing-element</h1>}
      />
    </Switch>
  );
};

export default Router;
