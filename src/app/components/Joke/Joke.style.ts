import styled from "styled-components";
import { Colors, DefaultBoxShadow } from "../../../style";

export const JokeComponent = styled.div`
  .joke-container {
    ${DefaultBoxShadow};
    background-color: ${Colors.mainHeaderBackground};
    border-radius: 15px;
    padding: 15px;

    p {
      color: ${Colors.mainFont};
      font-size: 20px;
      justify-content: center;
    }
  }

  .button-container {
    display: flex;
    justify-content: flex-end;
    margin-top: 30px;
  }
`;
