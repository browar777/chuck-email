import React, {
  forwardRef,
  ForwardRefRenderFunction,
  FunctionComponent,
  Ref,
  useEffect,
  useImperativeHandle,
} from "react";
import { IJoke, IRequestRespon, JokeService } from "../../../services";
import { useFetch } from "../../hooks";
import * as Styled from "./Joke.style";
import { DefaultButton, Loader } from "../index";

const Joke: ForwardRefRenderFunction<any> = (props: any, ref: Ref<any>) => {
  const jokeFetch = useFetch<JokeService, IJoke>(JokeService);

  useEffect(() => {
    loadJoke();
  }, []);

  const loadJoke = () => {
    jokeFetch.fetch(() => jokeFetch.service.getRandom());
  };

  useImperativeHandle(ref, () => jokeFetch.state?.value?.joke);

  return (
    <Styled.JokeComponent>
      <div className={"joke-container"}>
        <p>{jokeFetch.state?.value?.joke}</p>
      </div>
      <div className={"button-container"}>
        <DefaultButton callback={loadJoke}>
          {jokeFetch.loading ? <Loader /> : "Next joke"}
        </DefaultButton>
      </div>
    </Styled.JokeComponent>
  );
};

export default forwardRef(Joke);
