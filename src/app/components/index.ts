export { default as Table } from "./Table/Table";
export { default as EmailTable } from "./EmailTable/EmailTable";
export { default as DefaultInput } from "./inputs/DefaultInput/DefaultInput";
export { default as DefaultButton } from "./buttons/DefaultButton/DefaultButton";
export { default as Joke } from "./Joke/Joke";
export { default as Loader } from "./Loader/Loader";
