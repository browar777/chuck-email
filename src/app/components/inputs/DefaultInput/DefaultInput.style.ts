import styled from "styled-components";
import { Colors, DefaultBoxShadow, Fonts } from "../../../../style";

export const DefaultInput = styled.input`
  ${DefaultBoxShadow};
  background-color: ${Colors.mainHeaderBackground};
  font: 20px ${Fonts.MainFont};
  color: ${Colors.mainFont};
  border-radius: 10px;
  height: 40px;
  border: none;
  min-width: 140px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 15px;

  &:focus {
    outline: none !important;
  }
`;
