import {
  FunctionComponent,
  InputHTMLAttributes,
  LegacyRef,
  Ref,
  RefAttributes,
  useEffect,
} from "react";

import * as Styled from "./DefaultInput.style";

interface IDefaultInput extends InputHTMLAttributes<any> {
  refs: Ref<any>;
}

const DefaultInput: FunctionComponent<IDefaultInput> = (props): JSX.Element => {
  const { refs, ...attr } = props;
  return <Styled.DefaultInput ref={refs} {...attr} />;
};

export default DefaultInput;
