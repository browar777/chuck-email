import styled from "styled-components";
import { Colors } from "../../../style";

export const TableWrapper = styled.div`
  width: 100%;

  table {
    width: 100%;
    color: ${Colors.mainFont};
    font-size: 20px;

    thead {
      text-align: left;
    }
  }
`;
