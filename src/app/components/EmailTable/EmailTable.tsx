import {
  forwardRef,
  ForwardRefRenderFunction,
  Ref,
  useImperativeHandle,
  useState,
} from "react";
import { DefaultButton, DefaultInput, Table } from "../../components";
import { useForm } from "react-hook-form";
import { CellProps, Row } from "react-table";
import * as Styled from "./EmailTable.style";
export interface ICollectEmails {
  email: string;
  name: string;
  domain: string;
}
interface IEmailForm {
  email: string;
}
const EmailTable: ForwardRefRenderFunction<any> = (
  props: any,
  ref: Ref<any>
) => {
  const { register, handleSubmit, reset } = useForm();
  const [collectEmailsState, setCollectEmailsState] = useState<
    ICollectEmails[]
  >([]);

  const emailColumns = [
    {
      Header: "Name",
      accessor: "name",
    },
    {
      Header: "Domain",
      accessor: "domain",
    },
    {
      Header: "",
      accessor: "email",
      Cell: (value: CellProps<ICollectEmails>) => (
        <p
          className={"remove-row-button"}
          onClick={() => removeEmail(value.row.index)}
        >
          remove
        </p>
      ),
    },
  ];

  const removeEmail = (index: number) => {
    const emailCopy = [...collectEmailsState];
    emailCopy.splice(index, 1);

    setCollectEmailsState(emailCopy);
  };

  const submitForm = (data: IEmailForm) => {
    for (const collectEmail of collectEmailsState) {
      if (collectEmail.email == data.email) {
        alert(`Email ${data.email} already exist`);
        return;
      }
    }

    const emailSplit = data.email.split("@");
    setCollectEmailsState([
      ...collectEmailsState,
      {
        email: data.email,
        name: emailSplit[0],
        domain: emailSplit[1],
      },
    ]);

    reset();
  };

  useImperativeHandle(ref, () => collectEmailsState);

  return (
    <Styled.EmailTableWrapper>
      <form onSubmit={handleSubmit(submitForm)}>
        <DefaultInput
          name={"email"}
          type={"email"}
          required={true}
          refs={register}
          placeholder={"Email"}
        />
        <DefaultButton type={"submit"} callback={() => {}}>
          Add Email +
        </DefaultButton>
      </form>

      <Table columns={emailColumns} data={collectEmailsState} />
    </Styled.EmailTableWrapper>
  );
};

export default forwardRef(EmailTable);
