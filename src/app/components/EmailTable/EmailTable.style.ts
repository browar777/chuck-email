import styled from "styled-components";
import { Colors } from "../../../style";

export const EmailTableWrapper = styled.div`
  width: 100%;
  max-height: 400px;
  overflow: auto;

  form {
    width: 100%;
    margin-bottom: 20px;
    display: flex;

    input {
      margin-right: 20px;
    }
  }

  .remove-row-button {
    text-decoration: underline;
    margin: 0px;
    cursor: pointer;
  }
`;
