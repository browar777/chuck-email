import styled from "styled-components";
import { Colors } from "../../../style";

export const SvgLoader = styled.svg`
  width: 30px;
  height: 30px;
  path {
    fill: ${Colors.mainFont};
  }
`;
