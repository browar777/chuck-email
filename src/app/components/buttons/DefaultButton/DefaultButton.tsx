import { ButtonHTMLAttributes, FunctionComponent, useEffect } from "react";
import * as Styled from "./DefaultButton.style";

interface IDefaultButton extends ButtonHTMLAttributes<any> {
  children: any;
  callback?(): void;
}
const DefaultButton: FunctionComponent<IDefaultButton> = (props) => {
  return (
    <Styled.DefaultButton onClick={props.callback}>
      {props.children}
    </Styled.DefaultButton>
  );
};

export default DefaultButton;
