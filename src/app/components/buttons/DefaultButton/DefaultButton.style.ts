import styled from "styled-components";
import { Colors, DefaultBoxShadow, Fonts } from "../../../../style";

export const DefaultButton = styled.button`
  ${DefaultBoxShadow};
  background-color: ${Colors.mainHeaderBackground};
  font: 20px ${Fonts.MainFont};
  color: ${Colors.mainFont};
  border-radius: 10px;
  height: 40px;
  border: none;
  min-width: 140px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;

  &:focus {
    outline: none !important;
  }
`;
